const Path = require('./path.js');
const webpack = require('webpack');

module.exports = {
  module: {
    noParse: [/test/, /dist/],
    rules: [{
        test: /\.jsx?$/,
        exclude: /node_modules|\.git/,
        use: [{
            loader: 'babel-loader',
            query: {
                presets: [
                    'react',
                    'es2015',
                    'stage-0',
                ],
                plugins: [
                    'react-html-attrs',
                    'transform-class-properties',
                    'transform-decorators-legacy',
                ],
            },
        }],
    },
    {
        test: /\.(jpe?g|png|gif|svg)$/i,
        loader: "file-loader?name=/images/[name].[ext]",
    },
    ],
  },

  output: {
    path: Path.src,
    filename: 'bundle.js',
  },

  resolve: {
    extensions: ['.js', '.jsx', '.json'],
    modules: [
        Path.src,
        Path.node_modules,
    ],
    alias:{
        'Path': __dirname + '/path.js',
        'api': Path.api,
        'app': Path.app,
        'conf': Path.conf,
        'helpers': Path.helpers,
        'layouts': Path.layouts,
        'components': Path.components,
        'stylesBase.css': Path.styles_base,
        'stylesResponsive.css': Path.styles_responsive,
        'store': Path.store,
        'reducers': Path.reducers,
    }
  },

  plugins: [
        new webpack.NamedModulesPlugin(),
  ],

};
