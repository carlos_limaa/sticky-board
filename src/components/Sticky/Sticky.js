import svgIfClose from '!!file-loader!../../svg/if_close.svg';

import React from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import store from 'store';
import st from './sticky.css';
import classNames from 'classNames';

@connect((store) => ({}))
export default class Sticky extends React.Component {

    constructor(props) {
        super(props);
        const xRandom = this.props.x + (Math.floor((Math.random() * 100) + 1));
        const yRandom = this.props.y + (Math.floor((Math.random() * 100) + 1));
        this.state = {
            flip: false,
            dragging: false,
            rel: null,
            pos: { x: 0, y: 0 },
            style: {
                top: `${xRandom}px`,
                left: `${yRandom}px`,
            },
            translation: {},
            value: undefined,
        };
        this.onMouseUp = this.onMouseUp.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.onMouseDown = this.onMouseDown.bind(this);
        this.onMouseMove = this.onMouseMove.bind(this);
    }

    componentDidMount() {
        this.nameInput.focus();
    }

    handleClick(e) {
        const width = document.body.clientWidth;
        const heigth = document.documentElement.scrollHeight;
        const styleTop = Number(this.state.style.top.slice(0, -2)); // 20px -> 20
        const styleLeft = Number(this.state.style.left.slice(0, -2));
        if (!this.state.flip) {
            this.setState({
                translation: {
                    transform: 'rotateY(180deg)',
                },
                flip: true });
        } else {
            this.setState({ translation: { transform: 'rotateY(0deg)' }, flip: false });
        }
        this.nameInput.focus();
    }

    onMouseDown(e) {
        if (!this.state.dragging) {
            document.addEventListener('mousemove', this.onMouseMove);
            document.addEventListener('mouseup', this.onMouseUp);
        }
        if (e.button !== 0) return;
        const pos = ReactDOM.findDOMNode(this).getBoundingClientRect();
        this.setState({
            dragging: true,
            rel: {
                x: e.pageX - pos.left,
                y: e.pageY - pos.top,
            }
        });
        e.stopPropagation();
        e.preventDefault();
    }

    onMouseUp(e) {
        if (this.state.dragging) {
            document.removeEventListener('mousemove', this.onMouseMove);
            document.removeEventListener('mouseup', this.onMouseUp);
            this.setState({ dragging: false });
        }
        e.stopPropagation();
        e.preventDefault();
    }

    onMouseMove(e) {
        if (!this.state.dragging || this.state.flip) return;
        this.setState({
            pos: {
                x: e.pageX - this.state.rel.x,
                y: e.pageY - this.state.rel.y,
            },
            style: {
                top: `${e.pageY - this.state.rel.y}px`,
                left: `${e.pageX - this.state.rel.x}px`,
            },
        });
        e.stopPropagation();
        e.preventDefault();
    }

    render() {
        const { style, translation } = this.state;
        const stSticky = (this.state.flip) ? classNames(st.sticky, st.anim) : st.sticky;
        return (
            <div
                id="Sticky"
                style={ style }
                className={ stSticky }
                onDoubleClick={ this.handleClick }
                onMouseDown={ this.onMouseDown }
                onMouseUp={ this.onMouseUp }
            >

                <div className={ st.card } style={ translation }>

                    <div className={ st.cardFront }>
                        <p>Hello I'm component!</p>
                    </div>
                    <div className={ st.cardBack }>
                        <img src={ svgIfClose } alt="close" className={ st.close } onClick={ this.handleClick } />

                        <h1>Back!!!</h1>
                        <p>The sticky is blocked</p>
                        <input
                            value={ this.state.value }
                            placeholder="type any string"
                            className={ st.input }
                            ref={ (input) => { this.nameInput = input; } }
                        />
                        <div className={ st.footer } >
                            <p className={ st.small }>*Yoy can press double click for go back.</p>
                        </div>
                    </div>

                </div>
            </div>
        );
    }

  }
