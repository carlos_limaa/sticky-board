export default function reducer(state = {
    error: false,
}, action) {
    switch (action.type) {

        case 'STICKY_ADD_ERROR': {
            return { ...state, error: action.payload };
        }

        case 'STICKY_REMOVE_ERROR': {
            return { ...state, error: false };
        }

    }
    return state;
}
