import Conf from 'conf';
import { Sticky } from 'api';

const api = new Sticky(Conf.baseUrl);

export function getSticky() {
    return (dispatch) => {
        dispatch({ type: 'HELPER_LOADING_ACTIVE' });
        const payload = {};
        dispatch({ type: 'HELPER_LOADING_INACTIVE' });
        return dispatch({ type: 'sticky', payload });
    };
}

export function loading(value) {
    if (value) {
        return { type: 'HELPER_LOADING_ACTIVE' };
    }
    return { type: 'HELPER_LOADING_INACTIVE' };
}
