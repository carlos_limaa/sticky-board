import store from 'store';
import React from 'react';
import { Provider, connect } from 'react-redux';
import { shallow, mount, render } from 'enzyme';
import renderer from 'react-test-renderer';
import Sticky from './Sticky';
import * as actions from './stickyActions';
import reducer from './stickyReducer';
import { sticky, API } from 'api';
import Conf from 'conf';

describe('Sticky', () => {
    // Variables available for all tests.
    const reducerState = store.getState().Sticky;

    describe('visual', () => {
        // Variables
        let enzymeComponent = null;
        let tree = null;
        beforeAll(() => {
            const element = <Provider store={ store }><Sticky /></Provider>;
            enzymeComponent = mount(element);
            tree = renderer.create(element).toJSON();
            expect(tree).toMatchSnapshot();
        });
        test('Sring - Acceso portal', () => {
            const str = 'Sticky Component created!';
            expect(enzymeComponent.find('#Sticky').text()).toContain(str);
        });
    });

    describe('reducers', () => {
        test('LOADING_ACTIVE', () => {
            const response = reducer(reducerState, { type: 'LOADING_ACTIVE' });
            expect(response.active).toEqual(true);
        });
        test('LOADING_INACTIVE', () => {
            const response = reducer(reducerState, { type: 'LOADING_INACTIVE' });
            expect(response.active).toEqual(false);
        });
    });

    describe('actions', () => {
        test('success:getSticky()', (done) => {
            const unsubscribe = store.subscribe(() => {
                const props = store.getState().Sticky;
                try {
                    expect(props.active).toEqual(true);
                } catch (e) {
                    console.log(e);
                    throw Error;
                }
                done();
            });
            store.dispatch(actions.getSticky());
        });
    });
});
