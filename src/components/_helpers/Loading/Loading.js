/* eslint no-unused-vars: ["off"] */
/* global store */
import React from 'react';
import { connect } from 'react-redux';
import store from 'store';
import st from './loading.css';

@connect((store) => ({
    active: store.Loading.active,
}))
export default class Loading extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            text: 'Loading...',
        };
    }

    render() {
        if (this.props.active) {
            return (
                <div className={ st.top } id="Loading">
                    <div className={ st.loader } />
                    <p className={ st.text }>{ this.state.text }</p>
                </div>);
        }
        return null;
    }
}
