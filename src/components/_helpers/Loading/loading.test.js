import store from 'store';
import React from 'react';
import { Provider } from 'react-redux';
import { shallow, mount, render } from 'enzyme';
import renderer from 'react-test-renderer';
import Loading from './Loading';
import reducer from './loadingReducer';
import * as actions from './loadingActions';

describe('Loading', () => {
    describe('visual', () => {
        // Variables
        let enzymeComponent = null;
        let tree = null;
        beforeAll(() => {
            const element = <Provider store={ store }><Loading /></Provider>;
            enzymeComponent = mount(element);
            tree = renderer.create(element).toJSON();
            expect(tree).toMatchSnapshot();
        });
        test('Render text', (done) => {
            const str = 'Loading';
            store.subscribe(() => {
                expect(enzymeComponent.find('#Loading').text()).toContain(str);
                done();
            });
            store.dispatch(actions.active());
        });
    });
    describe('reducers', () => {
        // Variables available for all tests.
        const reducerState = store.getState().Loading;
        test('HELPER_LOADING_ACTIVE', () => {
            const response = reducer(reducerState, { type: 'HELPER_LOADING_ACTIVE' });
            expect(response.active).toEqual(true);
        });
        test('HELPER_LOADING_INACTIVE', () => {
            const response = reducer(reducerState, { type: 'HELPER_LOADING_INACTIVE' });
            expect(response.active).toEqual(false);
        });
    });
    describe('actions', () => {
        test('inactive()', (done) => {
            store.subscribe(() => {
                const props = store.getState().Loading;
                try {
                    expect(props.active).toEqual(false);
                } catch (e) {
                    throw Error(e);
                    console.log(e);
                }
                done();
            });
            store.dispatch(actions.inactive());
        });
    });
});
