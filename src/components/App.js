/* eslint no-unused-vars: ["off"] */
/* global store */
import React from 'react';
import store from 'store';
import { connect } from 'react-redux';
import { Home } from 'layouts';

@connect((store) => ({}))
export default class App extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return <Home />;
    }
}
