import store from 'store';
import React from 'react';
import { Provider } from 'react-redux';
import { shallow, mount, render } from 'enzyme';
import renderer from 'react-test-renderer';
import Home from './Home';
import * as actions from './homeActions';
import reducer from './homeReducer';
import { sample, API } from 'api';
import Conf from 'conf';

describe('Home', () => {
    describe('visual', () => {
        // Variables
        let enzymeComponent = null;
        let tree = null;
        beforeAll(() => {
            const element = <Provider store={ store }><Home /></Provider>;
            enzymeComponent = mount(element);
            tree = renderer.create(element).toJSON();
            expect(tree).toMatchSnapshot();
        });
        test('Render text', () => {
            const str = 'component';
            expect(enzymeComponent.find('#Home').text()).toContain(str);
        });
    });

    describe('reducers', () => {
        // Variables available for all tests.
        const reducerState = store.getState().Home;
        test('HOME_ADD_ERROR', () => {
            const payload = { value: 1, id_test: 'test' };
            const response = reducer(reducerState, { type: 'HOME_ADD_ERROR', payload });
            expect(response.error).toEqual(payload);
        });
        test('HOME_REMOVE_ERROR', () => {
            const response = reducer(reducerState, { type: 'HOME_REMOVE_ERROR' });
            expect(response.error).toEqual(false);
        });
    });
    let props;
    let loadingProps;
    describe('actions', () => {
        beforeEach(() => {
            props = store.getState().Home;
            loadingProps = store.getState().Loading;
        });
        test('loading(true)', (done) => {
            store.subscribe(() => {
                loadingProps = store.getState().Loading;
                try {
                    expect(loadingProps.active).toEqual(true);
                } catch (e) {
                    throw Error(e);
                    console.log(e);
                }
                done();
            });
            store.dispatch(actions.loading(true));
        });
        test('loading(false)', (done) => {
            store.subscribe(() => {
                loadingProps = store.getState().Loading;
                try {
                    expect(loadingProps.active).toEqual(false);
                } catch (e) {
                    throw Error(e);
                    console.log(e);
                }
                done();
            });
            store.dispatch(actions.loading(false));
        });
    });
});
