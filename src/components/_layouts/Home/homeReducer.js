export default function reducer(state = {
    error: false,
    notifications: [],
}, action) {
    switch (action.type) {

        case 'HOME_ADD_ERROR': {
            return { ...state, error: action.payload };
        }

        case 'HOME_REMOVE_ERROR': {
            return { ...state, error: false };
        }

    }

    return state;
}
