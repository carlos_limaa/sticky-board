export function loading(value) {
    if (value) {
        return { type: 'HELPER_LOADING_ACTIVE' };
    }
    return { type: 'HELPER_LOADING_INACTIVE' };
}
