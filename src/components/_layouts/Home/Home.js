import svgIfAdd from '!!file-loader!../../../svg/if_add.svg';

import React from 'react';
import { connect } from 'react-redux';
import store from 'store';
import classNames from 'classnames';
import { Sticky } from 'components';
import responsive from 'stylesResponsive.css';
// import * as actions from './homeActions';
import st from './home.css';

@connect((store) => ({}))
export default class Home extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            stickies: [],
            defaultValues: [10, 20], // x,y
        };
        this.handlerClick = this.handlerClick.bind(this);
        this.showStickies = this.showStickies.bind(this);
    }

    handlerClick() {
        const { stickies, defaultValues } = this.state;
        stickies.push({ x: defaultValues[0], y: defaultValues[1] });
        this.setState({ stickies });
    }

    showStickies() {
        const { stickies } = this.state;
        return stickies.map((v, pos) => {
            return <Sticky key={ pos } x={ v.x } y={ v.y } />;
        });
    }

    render() {
        const laterals = classNames(responsive.col_1, responsive.col_m_1);
        const center = classNames(responsive.col_10, responsive.col_m_10);
        return (
            <div className={ responsive.row } id="Home">
                <div className={ laterals } />
                <div className={ center } >
                    { this.showStickies() }
                </div>
                <div className={ laterals } />
                <div className={ st.footer }>
                    <a onClick={ this.handlerClick } className={ st.addButton }>
                        <img
                            src={ svgIfAdd }
                            className={ st.ifAdd }
                            alt="Press here for add sticky"
                        />
                    </a>
                </div>
            </div>
        );
    }
}
