import { combineReducers } from 'redux';

import Sticky from './components/Sticky/stickyReducer';
import API from './api/apiReducer';
import Home from './components/_layouts/Home/homeReducer';
import Loading from './components/_helpers/Loading/loadingReducer';

export default combineReducers({
    API,
    Home,
    Loading,
    Sticky,
});
